import os
import json
import flask

settings_path = os.environ.get('SETTINGS_PATH', 'project_settings.json')
json_data = open(settings_path).read()
settings = json.loads(json_data)

params = {}
params['static_url_path'] = settings['STATIC_URL_PATH']
app = flask.Flask(__name__, **params)
app.config.update(settings)

@app.route('/')
def home():
    static_url = app.config['STATIC_URL_PATH']
    show_settings = app.config['SHOW_SETTINGS_ROUTE']
    return flask.render_template('index.html', **locals())

@app.route(app.config['SHOW_SETTINGS_ROUTE'])
def show_config():
    data = {
	'static_url_path': app.static_url_path,
	'static_folder': app.static_folder,
        'STATIC_FOLDER': app.config['STATIC_URL_PATH'],
	'MYSQL_DATABASE_URL': app.config['MYSQL_DATABASE_USER'],
    }
    resp = app.make_response(json.dumps(data, indent=4))
    resp.mimetype = "application/json"
    return resp

if __name__ == '__main__':
  app.run(debug=True)
