Flask with JSON settings
========================

A very simple example of using Flask reading settings from
a JSON file.

Before you try to run the example, to the following:

```
virtualenv --no-site-packages env
source env/bin/activate
pip install -r requirements.txt
cp sample_project_settings.json project_settings.json
```

For setting the path to the JSON file using an environment variable
you can do something like this:

```
SETTINGS_PATH=/etc/config.json python app.py
```


(c) 2015 - Antonio Ognio <antonio@ognio.com>
